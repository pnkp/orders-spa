import { Component, OnInit } from '@angular/core';
import {ClientService} from "../../_services/clients/client.service";
import {ActivatedRoute} from "@angular/router";
import {Client} from "../../_models/client";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  clients: Client[];
  clientsToContinue: Client[] = [];
  displayedColumns: string[] = ['name', 'client_code', 'main_address', 'id'];

  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
    ) { }

  ngOnInit() {
    this.route.data.subscribe( data => {
        this.clients = data['clients']._embedded.items;
      }
    );
    this.spinner.hide();
    this.clientsToContinue = this.filterClients();
  }

  filterClients(): Client[] {
    return this.clients.filter(c => c.main_address == null);
  }
}
