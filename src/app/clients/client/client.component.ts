import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClientService} from "../../_services/clients/client.service";
import {AlertifyService} from "../../_services/alertify.service";
import {Client} from "../../_models/client";
import {User} from "../../_models/user";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  client: Client;
  clientForm: FormGroup;
  isClientCreated: boolean = false;
  isRequest: boolean = false;

  constructor(
    private fb: FormBuilder,
    private clientService: ClientService,
    private alertify: AlertifyService
  ) { }

  ngOnInit() {
    this.initializeFormBuilder();
  }

  initializeFormBuilder() {
   this.clientForm = this.fb.group({
      client_code: ['', [
        Validators.required,
        Validators.pattern('[0-9]{1,10}')
      ]],
      name: ['', Validators.required],
      nip: ['', [
        Validators.required,
        Validators.pattern('[0-9]{10}')
      ]]
    });
  }

  createClient() {
    if (this.clientForm.valid) {
      this.isRequest = true;
      this.client = Object.assign({}, this.clientForm.value);
      this.clientService.createClient(this.client).subscribe(response => {
        this.client = Object.assign({}, response);
        this.isClientCreated = true;
        this.alertify.success('Dodałeś klienta');
      }, error => {
        this.isRequest = false;
        this.alertify.error(error);
      })
    }
  }
}
