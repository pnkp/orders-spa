import {Component, Input, OnInit} from '@angular/core';
import {Client} from '../../_models/client';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientAddressService} from '../../_services/clients/client-address.service';
import {ClientAddress} from '../../_models/client-address';
import {AlertifyService} from '../../_services/alertify.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-client-address',
  templateUrl: './client-address.component.html',
  styleUrls: ['./client-address.component.css']
})
export class ClientAddressComponent implements OnInit {
  @Input() clientInput?: Client;
  addressForm: FormGroup;
  client: Client = this.clientInput;
  clientId: number;
  isResponseSend = false;

  constructor(
      private fb: FormBuilder,
      private clientAddressService: ClientAddressService,
      private alertify: AlertifyService,
      private activatedRoute: ActivatedRoute,
      private router: Router
    ) { }

  ngOnInit() {
    this.initializeForm();
    this.activatedRoute.params.subscribe(params => {
      this.clientId = params['id'];
    });
  }


  initializeForm() {
    this.addressForm = this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      postcode: ['', [
        Validators.required,
        Validators.pattern('^([0-9]{2})(-)([0-9]{3})+$')]
      ],
      isMainAddress: ['']
    });
  }

  createAddress() {
    if (this.addressForm.valid) {
      this.isResponseSend = true;
      const clientAddress: ClientAddress = Object.assign({}, this.addressForm.value);

      if (this.clientId > 0) {
        this.clientAddressService.createClientAddress(this.clientId, clientAddress).subscribe(
          (response: ClientAddress) => {
            if (this.addressForm.get('isMainAddress').value) {
              this.clientAddressService.setClientMainAddress(this.clientId, response).subscribe(() => {
                this.alertify.success('Dodałeś adres');
                this.router.navigate(['/klienci/dostawy', this.clientId]);
              });
            }
          }, error => {
            this.alertify.error(error);
          });
        return;
      }

      if (this.clientInput) {
        this.clientAddressService.createClientAddress(this.clientInput.id, clientAddress).subscribe(
          (response: ClientAddress) => {
            if (this.addressForm.get('isMainAddress').value) {
              this.clientAddressService.setClientMainAddress(this.clientInput.id, response).subscribe(() => {
                this.alertify.success('Dodałeś adres');
                this.router.navigate(['/klienci/dostawy', this.clientInput.id]);
              });
            }
          }, error => {
            this.alertify.error(error);
          });
      }
    }
  }
}
