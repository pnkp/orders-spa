import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDeliveryListComponent } from './client-delivery-list.component';

describe('ClientDeliveryListComponent', () => {
  let component: ClientDeliveryListComponent;
  let fixture: ComponentFixture<ClientDeliveryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDeliveryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDeliveryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
