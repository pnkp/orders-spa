import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ClientAddress} from "../../../_models/client-address";

@Component({
  selector: 'app-client-delivery-list',
  templateUrl: './client-delivery-list.component.html',
  styleUrls: ['./client-delivery-list.component.css']
})
export class ClientDeliveryListComponent implements OnInit {
  clientId: number;
  addresses: ClientAddress[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.addresses = data['address'];
    });
    this.route.params.subscribe(params => {
      this.clientId = params['id']
    });
  }

  getDeliveryAddress() {

  }
}
