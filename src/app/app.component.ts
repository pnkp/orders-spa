import {Component, OnInit} from '@angular/core';
import {TokenStorage} from "./_token/token-storage";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'order-register-spa';

  constructor(private tokenStorage: TokenStorage) {}

  ngOnInit() {
    if (TokenStorage.getUser()) {
      this.tokenStorage.currentUser = TokenStorage.getUser();
    }
  }
}
