import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError} from "rxjs/operators";
import {Unit} from "../_models/unit";
import {UnitService} from "../_services/products/unit.service";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UnitResolver implements Resolve<Unit[]> {
  constructor(private unitService: UnitService,) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Unit[]> {
    return this.unitService.getUnits()
      .pipe(
        catchError(error => {
          console.log(error);
          return of(null);
        })
      );
  }
}