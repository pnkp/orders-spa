import {Injectable} from "@angular/core";
import {ClientService} from "../_services/clients/client.service";
import {Resolve} from "@angular/router";
import {Client} from "../_models/client";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClientResolver implements Resolve<Client[]> {
  constructor(
    private clientService: ClientService,
    private spinner: NgxSpinnerService
    ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Client[]> {
    this.spinner.show();
    return this.clientService.getClientsOfUser()
    .pipe(
      catchError(error => {
        console.log(error);
        return of(null);
      })
    );
  }
}
