import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {OrderBody} from '../_models/order-body';
import {OrderBodyService} from '../_services/orders/order-body.service';

@Injectable({
  providedIn: 'root'
})
export class OrderBodyResolver implements Resolve<OrderBody[]> {
  constructor(private orderService: OrderBodyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderBody[]> {
    const id: number = route.params.id;

    return this.orderService.getOrdersBody(id);
  }
}
