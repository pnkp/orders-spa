import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError} from "rxjs/operators";
import {ProductService} from "../_services/products/product.service";
import {Product} from "../_models/product";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductResolver implements Resolve<Product[]> {
  constructor(
    private productService: ProductService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product[]> {
    return this.productService.getProducts()
      .pipe(
        catchError(error => {
          console.log(error);
          return of(null);
        })
      );
  }
}