import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError} from "rxjs/operators";
import {ClientAddress} from "../_models/client-address";
import {ClientAddressService} from "../_services/clients/client-address.service";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClientAddressResolver implements Resolve<ClientAddress> {
  constructor(
    private clientAddressService: ClientAddressService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ClientAddress> {
    let id = route.params.id;

    return this.clientAddressService.getClientAddress(id)
      .pipe(
        catchError(error => {
          console.log(error);
          return of(null);
        })
      );
  }
}