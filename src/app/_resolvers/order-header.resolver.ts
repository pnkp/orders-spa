import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {catchError} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {OrderHeader} from "../_models/order-header";
import {OrderHeaderService} from "../_services/orders/order-header.service";

@Injectable({
  providedIn: 'root'
})
export class OrderHeaderResolver implements Resolve<OrderHeader[]> {
  constructor(
    private orderService: OrderHeaderService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderHeader[]> {

    return this.orderService.getOrdersHeader()
      .pipe(
        catchError(error => {
          console.log(error);
          return of(null);
        })
      );
  }
}