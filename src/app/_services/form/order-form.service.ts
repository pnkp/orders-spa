import {Injectable} from '@angular/core';
import {SortOrderParams} from "../../_models/sort-order-params";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DateFormatterService} from "../../_services/date/date-formatter.service";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class OrderFormService {
  private readonly formName = 'orderForm';

  constructor(private router: Router) {
  }

  public saveFilteredForm(form: SortOrderParams): void {
    sessionStorage.setItem(this.formName, JSON.stringify(form, undefined, 2));
  }

  public getSavedForm(): SortOrderParams {
    let sessionForm: SortOrderParams = JSON.parse(sessionStorage.getItem(this.formName));

    if (!sessionForm) {
      return null;
    }

    return {
      dateFrom: new Date(sessionForm.dateFrom),
      dateTo: new Date(sessionForm.dateTo),
      route: sessionForm.route,
      sortBy: sessionForm.sortBy,
      isReady: sessionForm.isReady
    }
  }

  public setQueryParams(formParams: SortOrderParams, activatedRoute: ActivatedRoute): void {
    const queryParams = this.parseDateToString(formParams);
    this.router.navigate(['.'], {relativeTo: activatedRoute, queryParams: queryParams})
  }

  public navigateToSavedForm(activatedRoute: ActivatedRoute): Observable<SortOrderParams> {
    return activatedRoute.queryParams.pipe(map(queryParams => {
      queryParams = this.parseDateToString(queryParams);
      this.router.navigate(['.'], {relativeTo: activatedRoute, queryParams: queryParams});

      return {
        dateFrom: DateFormatterService.toDate(queryParams.dateFrom),
        dateTo: DateFormatterService.toDate(queryParams.dateTo),
        route: queryParams.route,
        sortBy: queryParams.sortBy,
        isReady: queryParams.isReady === 'true'
      }
    }));
  }

  private parseDateToString(queryParams: Params): SortOrderParams {
    const obj = Object.keys(queryParams);
    obj.forEach(qpKeys => {
      if (queryParams[qpKeys] instanceof Date) {
        queryParams[qpKeys] = DateFormatterService.format(queryParams[qpKeys])
      }
    });

    return queryParams;
  }
}