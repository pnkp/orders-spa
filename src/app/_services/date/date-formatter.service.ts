import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateFormatterService {

  public static format(date: Date): string {
    return `${date.getDate().toString()}-${(date.getMonth() + 1).toString()}-${date.getFullYear().toString()}`;
  }

  public static toDate(date: string): Date {
    const spitedDate: string[] = date.split('-');
    return new Date(Number(spitedDate[2]), Number(spitedDate[1]) - 1, Number(spitedDate[0]));
  }
}
