import { Injectable } from '@angular/core';
import {User} from "../../_models/user";
import {Observable} from "rxjs";
import {BaseUrl} from "../base.url";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(BaseUrl.baseUrl + 'users', user)
      .pipe(map((user: User) => {
        return user;
      }));
  }
}
