import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../../_models/product";
import {Observable} from "rxjs";
import {BaseUrl} from "../base.url";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  createProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(BaseUrl.baseUrl + 'products', product);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(BaseUrl.baseUrl + 'products');
  }
}
