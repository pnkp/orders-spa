import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BaseUrl} from "../base.url";
import {Unit} from "../../_models/unit";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor(private http: HttpClient) { }

  getUnits(): Observable<Unit> {
    return this.http.get<Unit>(BaseUrl.baseUrl + 'units');
  }
}
