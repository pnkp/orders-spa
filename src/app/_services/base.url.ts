import {environment} from "../../environments/environment";

export class BaseUrl {
  public static readonly baseUrl = environment.baseUrl;
  
}