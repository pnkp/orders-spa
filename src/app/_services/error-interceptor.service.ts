import { Injectable } from '@angular/core';
import {
  HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import {Observable, throwError} from "rxjs/index";
import {catchError} from "rxjs/internal/operators";
import {AlertifyService} from "./alertify.service";

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor {
  private errorCodes: number[] = [401, 404, 403, 500];

  constructor(private alertify: AlertifyService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(

      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          console.log(error);
          if (error.error != null && error.error.message != null) {
            let messageString: string = '';

            for (let message in error.error.message) {
              if (error.error.message.hasOwnProperty(message)) {
                messageString += messageString + message + ': ' + error.error.message[message] + '\n';
              }
            }
            return throwError(messageString);
          }

          if (this.errorCodes.includes(error.status)) {
            this.alertify.error(error.statusText);
            return throwError(error.statusText)
          }
        }
      })
    );
  }

}

export const ErrorInterceptorProvide = {
  provide: HTTP_INTERCEPTORS,
  useClass: ErrorInterceptorService,
  multi: true
};
