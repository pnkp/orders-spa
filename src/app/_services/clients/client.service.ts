import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Client} from '../../_models/client';
import {BaseUrl} from '../base.url';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clients: Client[];

  constructor(private http: HttpClient) { }

  createClient(client: Client) {
    return this.http.post<Client>(BaseUrl.baseUrl + 'clients', client);
  }

  deleteClient(client: Client) {
    return this.http.delete(BaseUrl.baseUrl + 'clients/' + client.id);
  }

  getClientsOfUser() {
    return this.http.get<Client[]>(BaseUrl.baseUrl + 'clients');
  }
}
