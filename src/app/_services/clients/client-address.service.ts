import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Client} from '../../_models/client';
import {ClientAddress} from '../../_models/client-address';
import {BaseUrl} from '../base.url';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientAddressService {
  delivery: ClientAddress[];

  constructor(private http: HttpClient) { }

  // createClientAddress(client: Client, clientAddress: ClientAddress): Observable<ClientAddress> {
  //   return this.http.post<ClientAddress>(BaseUrl.baseUrl + 'clients/' + client.id + '/address', clientAddress);
  // }

  createClientAddress(id: number, clientAddress: ClientAddress): Observable<ClientAddress> {
    return this.http.post<ClientAddress>(BaseUrl.baseUrl + 'clients/' + id + '/address', clientAddress);
  }

  // setClientMainAddress(client: Client, clientAddress: ClientAddress): Observable<Client> {
  //   return this.http.patch<Client>(
  //     BaseUrl.baseUrl + 'clients/' + client.id + '/address/' + clientAddress.id, {});
  // }

  setClientMainAddress(clientId: number, clientAddress: ClientAddress): Observable<Client> {
    return this.http.patch<Client>(
      BaseUrl.baseUrl + 'clients/' + clientId + '/address/' + clientAddress.id, {});
  }

  getClientAddress(clientId: number): Observable<ClientAddress> {
    return this.http.get<ClientAddress>(BaseUrl.baseUrl + 'clients/' + clientId + '/address');
  }
}
