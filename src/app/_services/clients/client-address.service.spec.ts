import { TestBed } from '@angular/core/testing';

import { ClientAddressService } from './client-address.service';

describe('ClientAddressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientAddressService = TestBed.get(ClientAddressService);
    expect(service).toBeTruthy();
  });
});
