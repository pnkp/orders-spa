import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../_models/user';
import {BaseUrl} from './base.url';
import {map} from 'rxjs/internal/operators';
import {Token} from '../_token/token';
import {TokenStorage} from '../_token/token-storage';
import {JwtHelperService} from '@auth0/angular-jwt';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorage,
    private jwtHelper: JwtHelperService
  ) { }

  loginStatus = new BehaviorSubject<boolean>(false);
  currentLoginStatus = this.loginStatus.asObservable();

  changeLoginStatus(status: boolean) {
    this.loginStatus.next(status);
  }

  isTokenExpired(): boolean {
    const decodedToken: Token = this.jwtHelper.decodeToken(TokenStorage.getToken());
    if (!decodedToken) {
      return false;
    }

    const unixActualTime: number = Math.ceil(Date.now() / 1000);

    if (!decodedToken.exp) {
      return true;
    }

    return unixActualTime > decodedToken.exp;
  }

  loginUser(user: User) {
    if (this.isTokenExpired() || TokenStorage.getToken()) {
      this.tokenStorage.removeToken();
      this.tokenStorage.removeUser();
    }

    const basicDecodedUser = 'Basic ' + window.btoa(user.username + ':' + user.password);
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization' : basicDecodedUser
      })
    };

    return this.http.post(BaseUrl.baseUrl + 'tokens', null, httpOptions)
      .pipe(
        map((response: Token) => {
        this.tokenStorage.setToken(response);
        this.tokenStorage.setUser(response.user);
          this.changeLoginStatus(true);
        })
      );
  }

  isLoggedIn(): boolean {
    return TokenStorage.getToken() != null;
  }

  logout(): void {
    this.tokenStorage.removeToken();
    this.tokenStorage.removeUser();
    this.changeLoginStatus(false);
  }
}
