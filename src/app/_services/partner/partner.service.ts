import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PartnerRequest, PartnerResponse} from "../../_models/partner";
import {BaseUrl} from "../base.url";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root',
})
export class PartnerService implements IPartnerService {

    constructor(private http: HttpClient) {
    }

    public createPartner(partner: PartnerRequest): Observable<PartnerResponse> {
        return this.http.post<PartnerResponse>(`${BaseUrl.baseUrl}superadmin/partners`, partner);
    }

    getAllPartners(): Observable<PartnerResponse[]> {
        return undefined;
    }

    getPartner(): Observable<PartnerResponse> {
        return undefined;
    }
}

export interface IPartnerService {
    createPartner(partner: PartnerRequest): Observable<PartnerResponse>;
    getPartner(): Observable<PartnerResponse>;
    getAllPartners(): Observable<PartnerResponse[]>;
}
