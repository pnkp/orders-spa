import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OrderBody} from "../../_models/order-body";
import {Observable} from "rxjs";
import {BaseUrl} from "../base.url";
import {OrderHeader} from "../../_models/order-header";

@Injectable({
  providedIn: 'root'
})
export class OrderBodyService {
  constructor(
    private http: HttpClient
  ) { }

  createOrderBody(orderBody: OrderBody, orderHeader: OrderHeader): Observable<OrderBody> {
    return this.http.post<OrderBody>(BaseUrl.baseUrl + 'orders/' + orderHeader.id + '/body', orderBody);
  }

  getOrdersBody(orderHeaderId: number): Observable<OrderBody[]> {
    return this.http.get<OrderBody[]>(BaseUrl.baseUrl + 'orders/' + orderHeaderId + '/body');
  }
}
