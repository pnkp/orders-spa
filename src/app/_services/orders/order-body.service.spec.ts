import { TestBed } from '@angular/core/testing';

import { OrderBodyService } from './order-body.service';

describe('OrderBodyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderBodyService = TestBed.get(OrderBodyService);
    expect(service).toBeTruthy();
  });
});
