import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {OrderHeader} from '../../_models/order-header';
import {BaseUrl} from '../base.url';
import {Observable} from 'rxjs';
import {SortOrderParams} from "../../_models/sort-order-params";
import {Page} from "../../_models/page";
import {DateFormatterService} from "../date/date-formatter.service";

@Injectable({
  providedIn: 'root'
})
export class OrderHeaderService {

  constructor(private http: HttpClient) {
  }

  createOrderHeader(orderHeader: OrderHeader): Observable<HttpResponse<OrderHeader>> {
    return this.http.post<OrderHeader>(BaseUrl.baseUrl + 'orders/header', orderHeader, {observe: "response"});
  }

  getOrdersHeader(orderParams?: SortOrderParams, page?: Page) {
    let params = new HttpParams();

    if (null != orderParams) {
      if (orderParams.route.trim().length > 0) {
        params = params.append('route', orderParams.route);
      }
      params = params.append('dateFrom', orderParams.dateFrom.toString());
      params = params.append('dateTo', orderParams.dateTo.toString());
      params = params.append('isReady', orderParams.isReady ? '1' : '0');
      params = params.append('sortBy', orderParams.sortBy);
    }

    if (null != page) {
      params = params.append('limit', page.limit.toString());
      params = params.append('page', page.page.toString());
    }

    return this.http.get<OrderHeader[]>(BaseUrl.baseUrl + 'orders/header', {
      params
    });
  }

  deleteOrder(orderHeader: OrderHeader) {
    return this.http.delete(BaseUrl.baseUrl + 'orders/' + orderHeader.id + '/header');
  }
}
