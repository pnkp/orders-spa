import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../_services/auth.service';
import {User} from '../_models/user';
import {AlertifyService} from '../_services/alertify.service';
import {TokenStorage} from '../_token/token-storage';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {MediaMatcher} from '@angular/cdk/layout';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  private _user: User;
  private _isLogin: boolean;

  mobileQuery: MediaQueryList;
  model: any = {};

  constructor(
    private authService: AuthService,
    private alertifyService: AlertifyService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public tokenStorage: TokenStorage,
    public changeDetectorRef: ChangeDetectorRef,
    public media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.authService.currentLoginStatus.subscribe(
      login => this._isLogin = login
    );
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  login() {
    this.spinner.show();
    this.authService.loginUser(this.model).subscribe(() => {
      this.authService.currentLoginStatus.subscribe(
        login => this._isLogin = login
      );
      this._user = TokenStorage.getUser();
      this.alertifyService.success('Udało Ci się zalogować');
      this.spinner.hide();
    }, error => {
      this.alertifyService.error(error);
      this.spinner.hide();
    });
  }

  logout() {
    this.authService.logout();
    this.authService.currentLoginStatus.subscribe(
      login => this._isLogin = login
    );
    this.router.navigate(['']);
    this.alertifyService.message('Zostałeś wylogowany');
  }
}

