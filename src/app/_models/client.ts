import {User} from "./user";
import {ClientAddress} from "./client-address";
import {ClientDeliveryCode} from "./client-delivery-code";

export interface Client {
  id?: number;
  client_code?: string;
  name?: string;
  nip?: number;
  client_address?: ClientAddress[];
  main_address?: ClientAddress;
  client_delivery_code?: ClientDeliveryCode;
  user?: User;
}