import {Client} from "./client";
import {ClientDeliveryCode} from "./client-delivery-code";

export interface ClientAddress {
  id?: number;
  street: string;
  city: string;
  postcode: string;
  client?: Client;
  client_delivery_code?: ClientDeliveryCode
}