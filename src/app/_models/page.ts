export interface Page {
  page?: number;
  limit?: number;
  total?: number;
}
