import {ClientDeliveryCode} from "./client-delivery-code";

export interface OrderHeader {
  id?: number;
  delivery_code?: ClientDeliveryCode;
  route?: string;
  date?: string;
  date_stamp?: Date;
  description?: string;
  is_ready?: boolean;
}