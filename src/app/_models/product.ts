export interface Product {
  id?: number;
  product_code?: string;
  name?: string;
  units?: string;
}