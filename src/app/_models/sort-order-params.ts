export interface SortOrderParams {
  route?: string;
  dateFrom?: Date;
  dateTo?: Date;
  isReady?: boolean;
  sortBy?: string;
}
