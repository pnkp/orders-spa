export interface PartnerRequest {
    name: string;
}

export interface PartnerResponse {
    id: number;
    name: string;
}
