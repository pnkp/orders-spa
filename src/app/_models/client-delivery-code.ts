import {Client} from "./client";
import {ClientAddress} from "./client-address";

export interface ClientDeliveryCode {
  id?: number;
  client?: Client;
  client_address?: ClientAddress;
  code: string;
}