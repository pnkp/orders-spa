export interface User {
  id?: number;
  username: string;
  email?: string;
  password?: string;
  retypedPassword?: string;
  roles: string[];
}
