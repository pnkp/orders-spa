import {OrderHeader} from './order-header';
import {Product} from "./product";

export interface OrderBody {
  id?: number;
  order_header?: OrderHeader;
  product?: Product | any;
  price: number;
  description: string;
  quantity: number;
}
