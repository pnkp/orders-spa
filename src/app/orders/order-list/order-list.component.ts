import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {OrderHeader} from "../../_models/order-header";
import {OrderHeaderService} from "../../_services/orders/order-header.service";
import {AlertifyService} from "../../_services/alertify.service";
import {NgxSpinnerService} from "ngx-spinner";
import {DateAdapter, MAT_DATE_FORMATS, MatBottomSheet, MatTableDataSource, PageEvent} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MyDateAdapter} from "../../_adapters/my-date-adapter";
import {MY_DATE_FORMATS} from "../order-header-detail/order-header-detail.component";
import {SortType} from "../../_models/sort-type";
import {SortOrderParams} from "../../_models/sort-order-params";
import {Page} from "../../_models/page";
import {OrderFormService} from "../../_services/form/order-form.service";
import {OrderBodySheetComponent} from "../order-body-sheet/order-body-sheet.component";
import {DateFormatterService} from "../../_services/date/date-formatter.service";
import {OrderBodyService} from "../../_services/orders/order-body.service";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS},
  ]
})
export class OrderListComponent implements OnInit {
  private orders: OrderHeader[];
  private dataSource;
  private filterForm: FormGroup;
  private selectedFilters?: SortOrderParams;

  private displayedColumns: string[] = [
    'name',
    'delivery_code',
    'delivery_address',
    'delivery_date',
    'delivery_date_of_added',
    'order_id'
  ];
  private sortType: SortType[] = [
    {
      alias: 'rosnąco',
      type: 'asc',
    },
    {
      alias: 'malejąco',
      type: 'desc',
    }
  ];
  private page: Page = {};
  private isDataLoaded: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private orderService: OrderHeaderService,
    private alertify: AlertifyService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private orderFormService: OrderFormService,
    private bottomSheet: MatBottomSheet,
    private orderBodyService: OrderBodyService
  ) {
  }

  ngOnInit() {
    this.initializeForm();
    this.activatedRoute.data.subscribe(data => {
      this.orders = data['orders']._embedded.items;
      this.page.total = data['orders'].total;
      this.page.limit = data['orders'].limit;
      this.page.page = data['orders'].page;
      this.dataSource = new MatTableDataSource(this.orders);
    });

    this.orderFormService.navigateToSavedForm(this.activatedRoute).subscribe(params => {
      this.filterForm.setValue(params)
    });
  }

  initializeForm() {
    this.filterForm = this.fb.group({
      route: [''],
      dateFrom: [new Date(), Validators.required],
      dateTo: [new Date(), Validators.required],
      isReady: [false],
      sortBy: [this.sortType[0].type],
    }, {validator: this.isBoolean});
  }

  private isBoolean(g: FormGroup) {
    return typeof g.get('isReady').value === "boolean" ? null : {'missmatch': true}
  }

  filterRequest() {
    this.selectedFilters = Object.assign({}, this.filterForm.value);
    this.orderFormService.setQueryParams(this.selectedFilters, this.activatedRoute);

    this.isDataLoaded = false;
    this.orderService.getOrdersHeader(this.selectedFilters, this.page).subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(this.orders = res['_embedded'].items);
        this.page.total = res.total;
        this.page.limit = res.limit;
        this.page.page = res.page;
        this.isDataLoaded = true;
      },
      (err) => {
        this.isDataLoaded = true;
      }
    );
  }

  deleteOrder(order: OrderHeader) {
    this.isDataLoaded = false;
    this.orderService.deleteOrder(order).subscribe(() => {
        this.orders.splice(this.orders.findIndex(o => o.id === order.id), 1);
        this.dataSource = new MatTableDataSource(this.orders);
        this.spinner.hide();
        this.isDataLoaded = true;
      },
      error => {
        this.alertify.error(error);
        this.isDataLoaded = true;
      });
  }

  changePage(event: PageEvent) {
    this.page = {
      page: event.pageIndex + 1,
      limit: event.pageSize
    };
    this.isDataLoaded = false;
    this.orderService.getOrdersHeader(this.selectedFilters, this.page).subscribe(res => {
        this.dataSource = new MatTableDataSource(this.orders = res['_embedded'].items);
        this.page.total = res['total'];
        this.isDataLoaded = true;
      },
      (err) => {
        this.isDataLoaded = true;
      }
    );
  }

  openOrderSheet(order: OrderHeader) {
    this.orderBodyService.getOrdersBody(order.id).subscribe(res => {
      this.bottomSheet.open(OrderBodySheetComponent, {
        data: {res, order}
      });
    });
  }
}
