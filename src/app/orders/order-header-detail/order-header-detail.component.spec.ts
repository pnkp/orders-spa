import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHeaderDetailComponent } from './order-header-detail.component';

describe('OrderHeaderDetailComponent', () => {
  let component: OrderHeaderDetailComponent;
  let fixture: ComponentFixture<OrderHeaderDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderHeaderDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHeaderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
