import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderHeader} from '../../_models/order-header';
import {OrderHeaderService} from '../../_services/orders/order-header.service';
import {AlertifyService} from '../../_services/alertify.service';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material';
import {MyDateAdapter} from '../../_adapters/my-date-adapter';
import {HttpResponse} from "@angular/common/http";

export const MY_DATE_FORMATS = {
    parse: {
      dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    },
    display: {
      dateInput: 'input',
      monthYearLabel: { year: 'numeric', month: 'numeric' },
      dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
      monthYearA11yLabel: { year: 'numeric', month: 'long' },
    }
  };

@Component({
  selector: 'app-order-header-detail',
  templateUrl: './order-header-detail.component.html',
  styleUrls: ['./order-header-detail.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS},
  ],
})
export class OrderHeaderDetailComponent implements OnInit {
  deliveryCode: string;
  orderForm: FormGroup;
  isOrderAdded = false;
  isResponse = false;
  orderHeader?: OrderHeader;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private orderHeaderService: OrderHeaderService,
    private alertify: AlertifyService
  ) { }

  public static _getFormattedDate(date: Date): string {
    const month: number = date.getMonth();
    const correctMoth: number = month + 1;

    return date.getDate().toString() + '-'
      + correctMoth.toString() + '-' + date.getFullYear().toString();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.deliveryCode = params['id'];
    });
    this.initializeForm();
  }

  initializeForm() {
    this.orderForm = this.fb.group({
      delivery_code: [this.deliveryCode],
      route: ['', Validators.required],
      date: [new Date(), Validators.required],
      description: ['']
    });
  }

  createOrderHeader() {
    if (this.orderForm.valid) {
      this.isResponse = true;
      const orderHeader: OrderHeader = Object.assign({}, this.orderForm.value);
      orderHeader.date = OrderHeaderDetailComponent._getFormattedDate(this.orderForm.get('date').value);

      this.orderHeaderService.createOrderHeader(orderHeader).subscribe((response: HttpResponse<OrderHeader>) => {
        if (response.headers.get('Client-Today-Order')) {
          this.alertify.error('Ten klient posiada zamówienie na ten dzień');
        }

        this.orderHeader = response.body;
        this.isOrderAdded = true;
      }, error => {
        this.alertify.error(error);
        this.isResponse = false;
      });
    }
  }
}
