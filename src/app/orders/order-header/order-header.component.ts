import {Component, OnDestroy, OnInit} from '@angular/core';
import {Client} from '../../_models/client';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-order-header',
  templateUrl: './order-header.component.html',
  styleUrls: ['./order-header.component.css']
})
export class OrderHeaderComponent implements OnInit {
  clients: Client[];
  client: Client;

  constructor(
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.data.subscribe( data => {
        this.clients = data['clients']._embedded.items;
      }
    );
    this.spinner.hide();
  }

  selectClient(client: Client) {
    this.client = client;
  }
}
