import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {OrderBody} from "../../_models/order-body";

@Component({
  selector: 'app-order-body-list',
  templateUrl: './order-body-list.component.html',
  styleUrls: ['./order-body-list.component.css']
})
export class OrderBodyListComponent implements OnInit {
  orders: OrderBody[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      console.log(data);
      this.orders = data['orders'];
    })
  }

}
