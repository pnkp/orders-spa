import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderBodyListComponent } from './order-body-list.component';

describe('OrderBodyListComponent', () => {
  let component: OrderBodyListComponent;
  let fixture: ComponentFixture<OrderBodyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderBodyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBodyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
