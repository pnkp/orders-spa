import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClientAddress} from '../../_models/client-address';

@Component({
  selector: 'app-order-header-delivery',
  templateUrl: './order-header-delivery.component.html',
  styleUrls: ['./order-header-delivery.component.css']
})
export class OrderHeaderDeliveryComponent implements OnInit {
  addresses: ClientAddress[];
  clientId: number;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
        this.addresses = data['address'];
    });
  }
}
