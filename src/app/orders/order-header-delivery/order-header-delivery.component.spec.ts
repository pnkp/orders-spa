import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHeaderDeliveryComponent } from './order-header-delivery.component';

describe('OrderHeaderDeliveryComponent', () => {
  let component: OrderHeaderDeliveryComponent;
  let fixture: ComponentFixture<OrderHeaderDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderHeaderDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHeaderDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
