import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material";
import {OrderBody} from "../../_models/order-body";
import {OrderHeader} from "../../_models/order-header";

@Component({
  selector: 'app-order-body-sheet',
  templateUrl: './order-body-sheet.component.html',
  styleUrls: ['./order-body-sheet.component.css']
})
export class OrderBodySheetComponent implements OnInit {
  private _orderBody: OrderBody[];
  private _orderHeader: OrderHeader;

  constructor(
    private bottomSheetRef: MatBottomSheetRef<OrderBodySheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) private data: any,
  ) {
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  ngOnInit() {
    this._orderBody = this.data.res;
    this._orderHeader = this.data.order;
    console.log(this.data);
  }
}
