import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderBodySheetComponent } from './order-body-sheet.component';

describe('OrderBodySheetComponent', () => {
  let component: OrderBodySheetComponent;
  let fixture: ComponentFixture<OrderBodySheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderBodySheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBodySheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
