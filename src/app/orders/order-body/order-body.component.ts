import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderHeader} from '../../_models/order-header';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../_models/product';
import {OrderBodyService} from '../../_services/orders/order-body.service';
import {OrderBody} from '../../_models/order-body';
import {AlertifyService} from '../../_services/alertify.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material';

@Component({
  selector: 'app-order-body',
  templateUrl: './order-body.component.html',
  styleUrls: ['./order-body.component.css']
})
export class OrderBodyComponent implements OnInit {
  @Input() orderHeader?: OrderHeader;

  products?: Product[];
  orderForm: FormGroup;
  orderBodies?: OrderBody[] = [];
  productSelectedUnit?: string;
  isResponse = false;
  filteredProducts: Observable<Product[]>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private orderBodyService: OrderBodyService,
    private alertify: AlertifyService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.products = data['products'];
    });
    this.orderForm = this.initializeForm();
    this.filteredProducts = this.orderForm.get('product').valueChanges
      .pipe(
        startWith(''),
        map(value => this.filter(value))
      );
  }

  filter(value: string) {
    const filterValue = value.toLowerCase();
    return this.products.filter(opt => opt.name.toLowerCase().includes(filterValue));
  }

  initializeForm() {
    return this.fb.group({
      product: ['', Validators.required],
      price: ['', [
        Validators.pattern('([0-9]{1,10}(\.[0-9]{1,10})?)')
      ]],
      description: [''],
      quantity: ['', [
        Validators.required,
        Validators.pattern('([0-9]){1,10}')
      ]]
    });
  }

  createOrderBody() {
    if (this.orderBodies.length < 1) {
      return;
    }
    this.spinner.show();

    this.orderBodies.map(
      o => {
          return o.product = o.product.id;
      }
    );

    this.orderBodies.map(
      order => {
        this.orderBodyService.createOrderBody(order, this.orderHeader).subscribe(null,
          error => {
            this.alertify.error(error);
          });
      }
    );

    this.spinner.hide();
    this.router.navigate(['/twoje-zamowienia']);
  }

  addProductToOrder() {
    if (!this.orderForm.valid) {
      return;
    }

    const productCode: string = this.orderForm.get('product').value;
    const product: Product = this.products.find(
      p => p.product_code === productCode.split(',')[0]
    );

    if (this.orderBodies.includes(this.orderBodies.find(o => o.product.id === product.id))) {
      this.alertify.error('Ten produkt jest już na liście');
      return;
    }

    const price: number = this.orderForm.get('price').value;
    const description: string = this.orderForm.get('description').value;
    const quantity: number = this.orderForm.get('quantity').value;

    const orderBody: OrderBody = {
      price: price,
      description: description,
      quantity: quantity,
      product: product
    };

    this.orderBodies.push(orderBody);
  }

  removeSelectedProduct(product: Product) {
    this.orderBodies.splice(
      this.orderBodies.findIndex(p => p.id === product.id), 1
    );
  }

  displayUnitOfSelectedProduct(event: MatAutocompleteSelectedEvent) {
    let productName: string = event.option.value;
    productName = productName.split(',')[1];
    productName = productName.trim();

    const selectedProduct: Product = this.products.find((p: any) => {
      if (p.name === productName) {
        return p.units;
      }
    });

    this.productSelectedUnit = selectedProduct.units;
  }
}
