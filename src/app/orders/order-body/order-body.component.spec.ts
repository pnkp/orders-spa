import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderBodyComponent } from './order-body.component';

describe('OrderBodyComponent', () => {
  let component: OrderBodyComponent;
  let fixture: ComponentFixture<OrderBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
