import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Unit} from '../../_models/unit';
import {Product} from '../../_models/product';
import {ProductService} from '../../_services/products/product.service';
import {AlertifyService} from '../../_services/alertify.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productForm: FormGroup;
  units: Unit[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private productService: ProductService,
    private alertify: AlertifyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.units = data['units'];
    });
    this.initializeForm();
  }

  initializeForm() {
    this.productForm = this.fb.group({
      product_code: ['', Validators.required],
      name: ['', Validators.required],
      units: ['', Validators.required]
    });
  }

  createProduct() {
    if (this.productForm.valid) {
      const product: Product = Object.assign({}, this.productForm.value);
      this.productService.createProduct(product).subscribe(() => {
        this.alertify.success('Udało Ci się dodać produkt');
        this.router.navigate(['/']);
      }, error => {
        this.alertify.error(error);
      });
    }
  }
}
