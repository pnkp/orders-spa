import {Routes} from '@angular/router';
import {UserComponent} from './user/user.component';
import {ClientComponent} from './clients/client/client.component';
import {AuthGuard} from './_guards/auth.guard';
import {HomeComponent} from './home/home.component';
import {CanDeactivatedClientAddressGuard} from './_guards/candeactivated-client-address.guard';
import {ClientResolver} from './_resolvers/client.resolver';
import {ClientListComponent} from './clients/client-list/client-list.component';
import {ClientAddressComponent} from './clients/client-address/client-address.component';
import {ClientActionsComponent} from './clients/client-actions/client-actions.component';
import {ClientDeliveryListComponent} from './clients/client-delivery/client-delivery-list/client-delivery-list.component';
import {ClientAddressResolver} from './_resolvers/client-address.resolver';
import {OrderHeaderComponent} from './orders/order-header/order-header.component';
import {UnitResolver} from './_resolvers/unit.resolver';
import {ProductComponent} from './products/product/product.component';
import {OrderHeaderDeliveryComponent} from './orders/order-header-delivery/order-header-delivery.component';
import {OrderHeaderDetailComponent} from './orders/order-header-detail/order-header-detail.component';
import {ProductResolver} from './_resolvers/product.resolver';
import {OrderListComponent} from './orders/order-list/order-list.component';
import {OrderHeaderResolver} from './_resolvers/order-header.resolver';
import {OrderBodyListComponent} from './orders/order-body-list/order-body-list.component';
import {OrderBodyResolver} from './_resolvers/order-body.resolver';
import {CreatePartnerComponent} from "./partners/create-partner/create-partner.component";

export const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: 'klienci', component: ClientListComponent,
        resolve: { clients: ClientResolver } },
      { path: 'dodaj-klienta', component: ClientComponent },
      { path: 'klienci/dodaj-adres-dostawy/:id', component: ClientAddressComponent },
      { path: 'klienci/akcje/:id', component: ClientActionsComponent },
      { path: 'klienci/dostawy/:id', component: ClientDeliveryListComponent,
        resolve: { address: ClientAddressResolver } },
      { path: 'dodaj-zamowienie', component: OrderHeaderComponent,
        resolve: { clients: ClientResolver } },
      { path: 'dodaj-zamowienie/wybierz-dostawe/:id', component: OrderHeaderDeliveryComponent,
        resolve: { address: ClientAddressResolver } },
      { path: 'dodaj-zamowienie/szczegoly/:id', component: OrderHeaderDetailComponent,
        resolve: { products: ProductResolver } },
      { path: 'twoje-zamowienia', component: OrderListComponent,
        resolve: { orders: OrderHeaderResolver } },
      { path: 'twoje-zamowienie/szczegoly/:id', component: OrderBodyListComponent,
        resolve: { orders: OrderBodyResolver } },
      { path: 'dodaj-produkt', component: ProductComponent,
        resolve: { units: UnitResolver } },
      { path: 'partners', component: CreatePartnerComponent }
    ]
  },
  { path: '**', component: UserComponent }
];
