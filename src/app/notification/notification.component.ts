import { Component, OnInit } from '@angular/core';
import {BaseUrl} from '../_services/base.url';
import {TokenStorage} from '../_token/token-storage';

declare let EventSource: any;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  _source: EventSource;

  constructor() {
  }

  ngOnInit() {
    this.getNotification();
  }

  getNotification() {
    this._source.addEventListener('orders-notifications', event => {
      console.log(event);
    });
  }
}
