import { TestBed, async, inject } from '@angular/core/testing';

import {
  CanDeactivatedClientAddressGuard
} from './candeactivated-client-address.guard';

describe('CandeactivatedClientAddressGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanDeactivatedClientAddressGuard]
    });
  });

  it('should ...', inject([CanDeactivatedClientAddressGuard], (guard: CanDeactivatedClientAddressGuard) => {
    expect(guard).toBeTruthy();
  }));
});
