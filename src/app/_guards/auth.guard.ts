import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthService} from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (this.authService.isTokenExpired()) {
      this.authService.logout();
      this.authService.changeLoginStatus(false);
      this.router.navigate(['/']);
      return false;
    }

    if (this.authService.isLoggedIn()) {
      this.authService.changeLoginStatus(true);
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
