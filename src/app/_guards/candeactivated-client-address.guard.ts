import { Injectable } from '@angular/core';
import {CanDeactivate} from '@angular/router';
import {ClientAddressComponent} from "../clients/client-address/client-address.component";
import {ClientService} from "../_services/clients/client.service";

@Injectable({
  providedIn: 'root'
})
export class CanDeactivatedClientAddressGuard implements CanDeactivate<ClientAddressComponent> {
  constructor(private client: ClientService) {}

  canDeactivate(component: ClientAddressComponent): boolean {
    if (component.client != null) {
      let message: string = 'Jesteś pewny, że chcesz wyjść? Jeżeli wyjdziesz klient zostanie usunięty';
      if (confirm(message)) {
        this.client.deleteClient(component.client).subscribe();
        return true;
      }
    } else {
      return true
    }

    return false;
  }
}
