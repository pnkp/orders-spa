import {Token} from './token';
import {User} from '../_models/user';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenStorage {
  public static readonly Token = 'token';
  public static readonly User = 'user';
  public currentUser: User;

  public static getToken(): string | null {
    return localStorage.getItem(TokenStorage.Token);
  }

  public static getUser(): User {
    const userString: string = localStorage.getItem(TokenStorage.User);
    return JSON.parse(userString);
  }

  public setToken(token: Token): void {
    localStorage.setItem(TokenStorage.Token, token.token);
  }

  public setUser(user: User): void {
    localStorage.setItem(TokenStorage.User, JSON.stringify(user));
  }

  public removeToken(): void {
    localStorage.removeItem(TokenStorage.Token);
  }

  public removeUser(): void {
    localStorage.removeItem(TokenStorage.User);
  }
}
