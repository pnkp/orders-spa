import {User} from '../_models/user';

export interface Token {
  token: string;
  tokenType: string;
  tokenExp: number;
  user: User;
  exp: number;
}
