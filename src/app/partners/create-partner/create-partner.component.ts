import {Component, OnInit} from '@angular/core';
import {PartnerService} from "../../_services/partner/partner.service";

@Component({
    selector: 'app-create-partner',
    templateUrl: './create-partner.component.html',
    styleUrls: ['./create-partner.component.css'],
    providers: [PartnerService]
})
export class CreatePartnerComponent implements OnInit {

    constructor(private _partnerService: PartnerService) {
    }

    ngOnInit() {
    }

    public createPartner(): void {
        this._partnerService.getPartner();
    }
}
