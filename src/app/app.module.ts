import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {UserComponent} from './user/user.component';
import {routes} from './routes';
import {HomeComponent} from './home/home.component';
import {NavComponent} from './nav/nav.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './_services/auth.service';
import {JwtModule} from '@auth0/angular-jwt';
import {TokenStorage} from './_token/token-storage';
import {HttpClientModule} from '@angular/common/http';
import {ErrorInterceptorProvide} from './_services/error-interceptor.service';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AlertifyService} from './_services/alertify.service';
import {NgxSpinnerModule} from 'ngx-spinner';
import {OrderHeaderComponent} from './orders/order-header/order-header.component';
import {ClientComponent} from './clients/client/client.component';
import {AuthGuard} from './_guards/auth.guard';
import {ClientAddressComponent} from './clients/client-address/client-address.component';
import {ClientAddressService} from './_services/clients/client-address.service';
import {ClientService} from './_services/clients/client.service';
import {CanDeactivatedClientAddressGuard} from './_guards/candeactivated-client-address.guard';
import {ClientListComponent} from './clients/client-list/client-list.component';
import {ClientActionsComponent} from './clients/client-actions/client-actions.component';
import {ClientDeliveryListComponent} from './clients/client-delivery/client-delivery-list/client-delivery-list.component';
import {ProductComponent} from './products/product/product.component';
import {ProductService} from './_services/products/product.service';
import {UnitService} from './_services/products/unit.service';
import {OrderHeaderDeliveryComponent} from './orders/order-header-delivery/order-header-delivery.component';
import {OrderHeaderDetailComponent} from './orders/order-header-detail/order-header-detail.component';
import {OrderBodyComponent} from './orders/order-body/order-body.component';
import {OrderBodyService} from './_services/orders/order-body.service';
import {OrderListComponent} from './orders/order-list/order-list.component';
import {OrderBodyListComponent} from './orders/order-body-list/order-body-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTreeModule
} from '@angular/material';
import {NotificationComponent} from './notification/notification.component';
import {OrderBodySheetComponent} from './orders/order-body-sheet/order-body-sheet.component';
import {CreatePartnerComponent} from './partners/create-partner/create-partner.component';
import {IPartnerService, PartnerService} from "./_services/partner/partner.service";

@NgModule({
    declarations: [
        AppComponent,
        UserComponent,
        HomeComponent,
        NavComponent,
        OrderHeaderComponent,
        ClientComponent,
        ClientAddressComponent,
        ClientListComponent,
        ClientActionsComponent,
        ClientDeliveryListComponent,
        ProductComponent,
        OrderHeaderDeliveryComponent,
        OrderHeaderDetailComponent,
        OrderBodyComponent,
        OrderListComponent,
        OrderBodyListComponent,
        NotificationComponent,
        OrderBodySheetComponent,
        CreatePartnerComponent
    ],
    entryComponents: [
        OrderBodySheetComponent
    ],
    imports: [
        ReactiveFormsModule,
        BrowserModule,
        NgxSpinnerModule,
        HttpClientModule,
        RouterModule.forRoot(routes),
        FormsModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: TokenStorage.getToken,
                whitelistedDomains: ['localhost:8080'],
                blacklistedRoutes: ['localhost/tokens']
            }
        }),
        BsDropdownModule.forRoot(),
        BsDatepickerModule.forRoot(),
        BrowserAnimationsModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatSortModule,
        MatListModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatTreeModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatTableModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatBottomSheetModule,
        MatDialogModule
    ],
    providers: [
        AuthService,
        TokenStorage,
        ErrorInterceptorProvide,
        AlertifyService,
        AuthGuard,
        ClientAddressService,
        ClientService,
        CanDeactivatedClientAddressGuard,
        ProductService,
        UnitService,
        OrderBodyService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
