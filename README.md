<<<<<<< HEAD
SPA - Zamawiacz
======
Aplikacja stworzona do obsługi stworzonego przeze mnie wczęsniej [REST API](https://github.com/pnkp/orders-php-rest-api).
Do stworzenia plikacji użyłem frameworka Angular 6. 

### Wykorzystane elementy Angulara 6
1. Resolvers, 
2. Guards, 
3. Services,
4. Routing,
5. ReactivesForms,
6. Directives, 
7. HttpInterceptors
=======
Rest Api - Zamawiacz
=========

Projekt stworzony z myślą o ułatwienie pracy przedstawicielą handlowym. 
Aplikacja ma na celu agregowanie i wysyłanie zamówień do odpowiednich oddziałów na e-maila. Aplikacj została napisana w PHP 7 oraz SYMFONY 3.4. Komunikacją z bazą danych zajmuje się Doctrine 2. Do autoryzacji wykorzystuje tokeny JWT. Do cachowania danych użyłem serwera Redis.

##### Aplikacja udostępnia End-Pointy, do zarządzania:
1. Produktami,
2. Klientami,
3. Adresami dostawy,
4. Zamówieniami (nagłówkami zamówien, jak i asortymentem)

Nachodzące funkcjonalności, można zobaczyć [tutaj](https://github.com/pnkp/orders-php-rest-api/issues?q=is%3Aopen+is%3Aissue+label%3AFeature)

##### Aplikacja korzysta z elementów frameworka SYMFONY 3.4:
1. ParamConverter,
2. Event i EventSubsciber,
3. Voters,
4. Security,
5. Authenticator,
6. Command,
7. Annotation,
8. Validator,

##### Komponenty dołączone do projekty:
1. JMSSerializer - serializacja i deserializacj danych,
2. FOSRest Bundle - tworzenie end-pointów,
3. Lexik JwtAuthentication Bundle - autoryzacja poprzez JWT,
4. Nelmio Cors Bundle - obsługa CORS,
5. Nelmio API Doc Bundle - dokumentacja end-pointów,
6. Predis - bibloteka do obsługi serwera Redis,
7. Hateoas Bundle - użyłem do paginacji zasobów
>>>>>>> 23ac78d1277e65113a171b0de10b964d4e78aeee
